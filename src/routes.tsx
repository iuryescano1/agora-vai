import React from 'react';
import { BrowserRouter as Router, Route, Routes as Switch } from 'react-router-dom';

import Home from './pages/Home';
import Pagetwo from './pages/Pagetwo';

export default function Routes(){
    return (
        <Router>
            <Switch>
                <Route path="/" element={<Home />} />
                <Route path="/pagetwo" element={<Pagetwo />} />
            </Switch>
        </Router>
    );
}