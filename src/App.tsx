import Routes from "./routes";

type Props = {};

export default function App({}: Props) {
  return <Routes />;
}