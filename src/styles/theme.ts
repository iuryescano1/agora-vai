import { extendTheme } from "@chakra-ui/react";

export const theme = extendTheme ({
        fonts: {
            heading: 'Roboto',
            body: 'Roboto',
        },

        styles:{
            global:{
                body:{
                    bg: "RGBA(0, 0, 0, 0.04)",
                    color: "gray.50",
                },
            },
        },
})