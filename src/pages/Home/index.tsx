import { Box, Flex, Text, Image, VStack, Stack, Button } from '@chakra-ui/react';
import { useState } from "react"
import { useNavigate } from 'react-router-dom';

 
export default function Home() {
  const [loading, setLoading] = useState(false) 
  const navigate = useNavigate();



function handleClick() {
  setLoading(true)
  setTimeout(() => {
    setLoading(false)
  }, 1500)
  
  
}
  return (
    <>

    <Flex 
            w ="100vw" 
            h ="100vh" 
            align="center" 
            justifyContent="center">


      <Image src="/Logo Hospital.png" alt="teste" w="6rem" left="-450px" top="-360px">
        
      </Image>


      <VStack spacing={"22px"} position = "relative" left= "-50px">
            
          <Image src="/Image2.png" alt="teste2"></Image>
          <Stack spacing={-2}>

            <Box padding={4}>
              <Text align={"center"} fontSize="26px" textColor={"black"} >
                A sua doação importa!
              </Text>
            </Box>

            <Text align={"center"} fontSize="20px" textColor={"black"} p="8px" opacity={"75%"} >
              Seja um solidário            
            </Text>

            <Text textColor={"black"} align={"center"} fontSize="20px" opacity={"75%"} p="4px">
              Doar sangue é um ato de amor
            </Text>
            
            <Text textColor={"black"} align={"center"} fontSize="20px" opacity={"75%"}>
              Se você é saudavel, tem entre 16 e 69 anos e mais de 50 kg, doe sangue regurlamente
            </Text>

          </Stack>

          <Text textColor={"black"} align={"center"} fontSize="25px">
              Entre na lista de doadores
          </Text>
                <Button isLoading= {loading} colorScheme='red' variant='solid' 
                onClick={() => {handleClick(); navigate('/Pagetwo')  }} size={"md"}
                h="38px" w="300px">
                    Quero ajudar
                </Button>
      </VStack>  
    </Flex>
  </>
  )
}

